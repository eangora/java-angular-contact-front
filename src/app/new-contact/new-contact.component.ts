import { Component, OnInit } from '@angular/core';
import { ContatService } from '../services/contat.service';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.css']
})
export class NewContactComponent implements OnInit {

  constructor(private contactService:ContatService) { }

  ngOnInit() {
  }

  onsaveContact(dataForm){
  
    this.contactService.SaveContact(dataForm).subscribe((data)=>{
      console.log(data);
    })

  }

}

export class Contat {
    content: Content[];
    pageable: Pageable;
    totalPages: number;
    totalElements: number;
    last: boolean;
    size: number;
    number: number;
    sort: Sort;
    numberOfElements: number;
    first: boolean;
    empty: boolean;
  static totalPages: any;
  }
  
  export class  Pageable {
    sort: Sort;
    offset: number;
    pageNumber: number;
    pageSize: number;
    unpaged: boolean;
    paged: boolean;
  }
  
  export class  Sort {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
  }
  
  export class Content {
    id: number;
    nom: string;
    prenom: string;
    email: string;
    tel: string;
    photo: string;
    dateNaissance: string;
  }
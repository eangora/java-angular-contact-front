import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
import { AboutComponent } from './about/about.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FormulaireContactComponent } from './formulaire-contact/formulaire-contact.component';
import { NewContactComponent } from './new-contact/new-contact.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';
const routes: Routes = [
  {path:'about',component:AboutComponent},
  {path: 'contact',component: ContactsComponent},
  {path:'nouveauContact',component:FormulaireContactComponent},
  {path:'contactnew',component:NewContactComponent},
  {path:'editContact/:id',component:EditContactComponent},
  {path:'',redirectTo:'/about',pathMatch:'full'},
  {path:'**',component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

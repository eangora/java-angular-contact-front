import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http' ;
import { Contacts } from '../models/Contacts';
import { Contat } from '../models/Contat';




@Injectable({
  providedIn: 'root'
})
export class ContatService {

 private _contactUrl = "http://localhost:8082/contact/chercherContacts?mc=&size=5&page=0";
contacts:any=Contacts;
  constructor( private _http: HttpClient) { }
  
  
  PostContact(Contacts : Contacts){
    return this._http.post(this._contactUrl,Contat);
  }


    

 //C'est un getContact
  SearchContact(motcle:string,size:number,Currentpage:number){
   
    return this._http.get("http://localhost:8082/contact/chercherContacts?mc="+motcle+"&size="+size+"&page="+Currentpage)
   
  };

  //C'est une méthode qui envoie les contacts au serveur mysql(enregistre contatc via formulaire)
  SaveContact(contact:Contat){
   
    return this._http.post("http://localhost:8082/contact/contacts",contact);
   
  };

  
  gettContact(){
    return this._http.get(this._contactUrl);
  }

  //Editer un contact
  GetContact(id:number){
   
    return this._http.get("http://localhost:8082/contact/contact/"+id);
   
  }

  //Editer méthode Update Contact
  UpdateContact(contacts:Contacts){
    return this._http.put("http://localhost:8082/contact/contacts/"+contacts.id,contacts);
   
  }
  //supprimer

  DeleteContact(id:number){
    return this._http.delete("http://localhost:8082/contact/contacte/"+id);
  }

  
}

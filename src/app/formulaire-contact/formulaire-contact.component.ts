import { Component, OnInit } from '@angular/core';
import { Contat } from '../models/Contat';
import { ContatService } from '../services/contat.service';
@Component({
  selector: 'app-formulaire-contact',
  templateUrl: './formulaire-contact.component.html',
  styleUrls: ['./formulaire-contact.component.css']
})
export class FormulaireContactComponent implements OnInit {
contact:any = new Contat();
//Affichage resultat après saisie et validation
mode:any=1;
  constructor(private contactService:ContatService) { }

  ngOnInit() {
  }

  //Sauvegarde des données formulairecontact dans le serveur Mysql backend
  saveContact(){
    this.contactService.SaveContact(this.contact).subscribe((data)=>{
      console.log(data);
      this.contact = data;
      this.mode = 2;
    })
   
  }

}

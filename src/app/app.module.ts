import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule} from '@angular/forms';
import { HttpModule} from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactsComponent } from './contacts/contacts.component';
import { AboutComponent } from './about/about.component';
// import { ContatService } from './services/contat.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ContatService } from './services/contat.service';
import { FormulaireContactComponent } from './formulaire-contact/formulaire-contact.component';
import { NewContactComponent } from './new-contact/new-contact.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';
// import { BookSearchComponent } from './models/book-search/book-search.component';


@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    AboutComponent,
    PageNotFoundComponent,
    FormulaireContactComponent,
    NewContactComponent,
    EditContactComponent
    // BookSearchComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule
    
  ],
  providers: [ContatService],
  bootstrap: [AppComponent]
})
export class AppModule { }

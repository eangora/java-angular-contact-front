import { Component, OnInit,EventEmitter,Input,Output } from '@angular/core';
import { Contat } from '../models/Contat';
import { ContatService } from '../services/contat.service';
import { Router } from '@angular/router';
import { Contacts } from '../models/Contacts';




@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
 
  contacts:any;
  motcle:string="";
  Currentpage:number=0;
  size:number=2;
 pages:Array<number>;
  
  //Editer un contact on fait appel à router
  constructor(private contactService:ContatService, private router:Router) { }

//Affichage données de la table Mysql apiRestjava(créer une classe.ts convertir avec jsontots)
  ngOnInit():void{
    // console.log("Initialisation ...");
      // this.contactService.gettContact().subscribe((data)=>{
      //     this.contacts = data as Contat[];
      // })

  
     }

//Recherche mot Cle
  searchMotCle(){
    console.log("Initialisation ...");
    this.contactService.SearchContact(this.motcle,this.size,this.Currentpage).subscribe((data)=>{
        this.contacts = data as Contat[];
        this.pages = new Array(this.contacts.totalPages);
    })

  }
  //Recherche mot clé
  chercher(){
    this.searchMotCle();
  }

  //Pagination
  gotoPage(i:number){
    this.Currentpage=i;
    this.searchMotCle();
  }
  //Méthode Editer Contact
  onEditContact(id:number){
    this.router.navigate(['/editContact',id]);

  }
  onSupprimerContact(liste:Contacts){
    let confirm=window.confirm('Voulez vous vraiment effectuer cette opération?');
    if (confirm ==true) {
      this.contactService.DeleteContact(liste.id).subscribe((data)=>{
        this.contacts.content.splice(this.contacts.content.indexOf(liste),1);
        // alert('la donnée a été supprimée avec succès!');
        //this.router.navigate(['contact']);
      })
      
      
    }
    
  }

}

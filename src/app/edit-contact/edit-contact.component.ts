import { Component, OnInit } from '@angular/core';
import { Contat } from '../models/Contat';
import { ActivatedRoute, Router } from '@angular/router';
import { ContatService } from '../services/contat.service';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {
  mode:number=1;
contact:any= new Contat();
idContact:number;
  constructor( activatedRoute:ActivatedRoute, private contactService:ContatService,private router:Router) { 
    // console.log('...............................................');
    // console.log(activatedRoute.snapshot.params['id']);//Onrecupère l'id
    // console.log('...............................................');
    this.idContact = activatedRoute.snapshot.params['id'];

  }
//Editer Contact
  ngOnInit() {
  this.contactService.GetContact(this.idContact).subscribe((data)=>{
    this.contact = data; 
  },err=>{
    console.log(err);
  })
  }

  //Editer un contact
  updateContact(){
    this.contactService.UpdateContact(this.contact).subscribe((data)=>{
      console.log(data);
      alert('mise à jour effectuée!');
      this.router.navigate(['contact']);//Permet de revenir sur la page contacts.component.html
      
    },err=>{
      console.log(err);
      alert('Problème');
    })

  }

  

}
